package main

import (
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"log"
	"time"
)

func main() {
	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}
	defer ui.Close()

	termWidth, termHeight := ui.TerminalDimensions()
	events := ui.PollEvents()
	ticker := time.NewTicker(time.Second).C
	drawClock(termWidth, termHeight)
	for {
		select {
		case e := <-events:
			switch e.ID {
			case "q", "<C-c>":
				return
			case "<Resize>":
				payload := e.Payload.(ui.Resize)
				ui.Clear()
				drawClock(payload.Width, payload.Height)
			}
		case <-ticker:
			drawClock(termWidth, termHeight)
		}

	}
}

func draw() {
	termWidth, termHeight := ui.TerminalDimensions()
	p1 := widgets.NewParagraph()
	p1.Border = false
	p1.Title = "Current Time:"
	p1.TextStyle = ui.NewStyle(ui.ColorGreen, ui.ColorBlack)
	startingLocX := int(2 + float32(termWidth) * 0.2)
	startingLocY := int(2 + float32(termHeight) * 0.2)
	p1.SetRect(startingLocX, startingLocY, startingLocX + 40, startingLocY + 20)

	t := time.Now().Format(time.RFC3339Nano)
	p1.Text = t
	ui.Render(p1)
}

func drawClock(termW, termH int) {
	hr, min, sec := time.Now().Clock()

	grid := ui.NewGrid()
	grid.SetRect(0, 0, termW, termH)

	grid.Set(
		ui.NewRow(1.0,
			// hours
			drawDots(hr / 10),
			drawDots(hr % 10),

			// minutes
			drawDots(min / 10),
			drawDots(min % 10),

			// seconds
			drawDots(sec / 10),
			drawDots(sec % 10),
		),
	)

	ui.Render(grid)
}

func drawDots(number int) ui.GridItem {
	first, second, third, fourth := false, false, false, false

	if number % 2 == 1 {
		first = true
	}

	if 	number == 2 ||
		number == 3 ||
		number == 6 ||
		number == 7 {
		second = true
	}

	if number > 3 && number < 8 {
		third = true
	}

	if number > 7 {
		fourth = true
	}

	col := ui.NewCol(1.0/6.0,
		ui.NewRow(1.0 / 4.0, drawDot(fourth)),
		ui.NewRow(1.0 / 4.0, drawDot(third)),
		ui.NewRow(1.0 / 4.0, drawDot(second)),
		ui.NewRow(1.0 / 4.0, drawDot(first)),
	)

	return col
}

func drawDot(shouldDraw bool) *widgets.Paragraph {
	p := widgets.NewParagraph()
	p.Border = false
	p.SetRect(0, 0, 1, 1)
	p.TextStyle = ui.NewStyle(ui.ColorGreen, ui.ColorBlack, ui.ModifierBold)
	if shouldDraw {
		p.Text = "O"
	}
	return p
}


